# Imports
from mcpi.minecraft import Minecraft


mc = Minecraft.create()
gamewin = False

# Game Loop
while not gamewin:
    players = mc.getOnlinePlayers()

    for i in players:
        pos = i.getTilePos()
        # blue win = 1652, 63, 1261
        if (abs(pos.x - 1652) < 0.9 and abs(pos.z - 1261) < 0.9):
            gamewin(red)
            gamewin = True
            break

        # blue win = 1564, 63, 1261
        if (abs(pos.x - 1564) < 0.9 and abs(pos.z - 1261) < 0.9):
            gamewin(blue)
            gamewin = True
            break

def gamewin(team):
    mc.postToChat("Team " + str(team) + " has won!")